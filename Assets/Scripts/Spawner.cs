using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [Header("Spawn")]
    [SerializeField] protected Transform spawnParent;
    [SerializeField] protected float spawnWidth;


    protected  Vector3 GetRandomSpawnPos()
    {
        Vector3 p = spawnParent.position;

        float midLength = spawnWidth / 2.0f;
        p.x += Random.Range(-midLength, midLength);

        return p;
    }
}
