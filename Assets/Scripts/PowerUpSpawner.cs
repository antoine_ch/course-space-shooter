using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpSpawner : Spawner
{
    [Header("PowerUpSpawner")]
    [SerializeField] private List<GameObject> powerUpPrefabs;
    [SerializeField] private float initialDelay = 10.0f;
    [SerializeField] private float spawnDelay = 20.0f;
    int powerUpId = 0;

    private void Start()
    {
        StartCoroutine(PowerUpSpawning());
    }

    private IEnumerator PowerUpSpawning()
    {
        while(LevelManager.instance.phase != EGamePhase.Boss)
        {
            yield return new WaitForSeconds(initialDelay);

            Vector3 randomStartPos = GetRandomSpawnPos();

            GameObject powerUp = powerUpPrefabs[powerUpId];

            Instantiate(powerUp, randomStartPos, spawnParent.rotation);

            powerUpId = Random.Range(0, powerUpPrefabs.Count);

            yield return new WaitForSeconds(spawnDelay);
        }
    }
}
