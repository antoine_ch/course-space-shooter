using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIText : MonoBehaviour
{
    [SerializeField] private TMP_Text text;
    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void SetText(string value)
    {
        text.text = value;

        if (animator)
        {
            animator.SetTrigger("animate");
        }

    }
}
