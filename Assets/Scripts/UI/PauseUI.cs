using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseUI : MonoBehaviour
{
    [SerializeField] private Slider sfxSlider;
    [SerializeField] private Slider musicSlider;
    [SerializeField] private Toggle postProcessingToggle;

    // Start is called before the first frame update
    void OnEnable()
    {
        sfxSlider.onValueChanged.AddListener(OnSfxValueChanged);
        musicSlider.onValueChanged.AddListener(OnMusicValueChanged);
        postProcessingToggle.onValueChanged.AddListener(OnPostProcessingValueChanged);
    }

    void OnDisable()
    {
        sfxSlider.onValueChanged.RemoveListener(OnSfxValueChanged);
        musicSlider.onValueChanged.RemoveListener(OnMusicValueChanged);
        postProcessingToggle.onValueChanged.RemoveListener(OnPostProcessingValueChanged);
    }



    void OnSfxValueChanged(float value)
    {
        SoundsManager.instance.SetSoundVolume(value, ESoundType.Sfx);
    } 
    
    void OnMusicValueChanged(float value)
    {
        SoundsManager.instance.SetSoundVolume(value, ESoundType.Music);
    }

    void OnPostProcessingValueChanged(bool value)
    {
        GameManager.instance.ActivatePostProcessing(value);
    }
}
