using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private GameObject explosion;

    float speed = 100.0f;
    int damageValue = 1;

    private void Start()
    {
        Destroy(gameObject, 5.0f);
    }

    public void Init(int _damage)
    {
        damageValue = _damage;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<Enemy>())
        {
            Enemy enemy = collision.gameObject.GetComponent<Enemy>();

            enemy.ApplyDamage(damageValue);

            Explode();
        }
    }

    public void Explode()
    {
        GameObject e = Instantiate(explosion, transform.position, transform.rotation);

        Destroy(e, 1.0f);

        Destroy(gameObject);
    }
}
