using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public enum ESoundType { Sfx, Music };

public class SoundsManager : MonoBehaviour
{
    public static SoundsManager instance = null;

    [Header("Mixer")]
    [SerializeField] private AudioMixer mixer;
    const string MIXER_MUSIC = "MusicVolume";
    const string MIXER_SFX = "SFXVolume";



    [Header("Music")]
    [SerializeField] private List<AudioClip> musicClips;
    [SerializeField] private AudioClip bossClip;
    [SerializeField] private AudioSource musicSource;
    private Animator musicAnimator;
    
    [Header("Fire Sounds")]
    [SerializeField] private List<AudioClip> fireClips;
    [SerializeField] private AudioSource fireSource;

    
    private float sfxVolume = 0.5f;
    private float musicVolume = 0.5f;

     
    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        musicAnimator = musicSource.GetComponent<Animator>();

        //musicSource.ignoreListenerPause = true;
        //musicSource.ignoreListenerVolume = true;

        PlayMusic();
    }

    public void PlayFireSound()
    {
        PlaySound(fireClips[Random.Range(0, fireClips.Count)], fireSource, ESoundType.Music);

    }

    private void PlayMusic()
    {
        PlaySound(musicClips[Random.Range(0, musicClips.Count)], musicSource, ESoundType.Music);
    }

    void PlaySound(AudioClip c, AudioSource s, ESoundType t)
    {
        s.Stop();
        s.volume = GetSoundVolume(t);
        s.clip = c;
        s.Play();
    }


    public float GetSoundVolume(ESoundType t)
    {
        switch(t)
        {
            case ESoundType.Music:
                return musicVolume;
            case ESoundType.Sfx:
                return sfxVolume;
            default:
                return 0;

        }
    }


    public void PauseMusic(bool state)
    {
        musicAnimator.SetBool("pitchDown", state);
    }


    public void SetSoundVolume(float v, ESoundType t)
    {
        switch (t)
        {
            case ESoundType.Music:
                musicVolume = v;
                mixer.SetFloat(MIXER_MUSIC, Mathf.Log10(v) * 20.0f);
                break;
            case ESoundType.Sfx:
                sfxVolume = v;
                mixer.SetFloat(MIXER_SFX, Mathf.Log10(v) * 20.0f);
                break;
            default:
                break;

        }
    }

    public void SetBossMusic()
    {
        musicSource.clip = bossClip;
        musicSource.Play();
    }


}
