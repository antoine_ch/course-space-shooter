using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EGamePhase { Wave1, Wave2, Wave3, Boss, End}

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance = null;


    public EGamePhase phase { private set; get; }

    [Header("Wave Length")]
    [SerializeField] private float wave1length;
    [SerializeField] private float wave2length;
    [SerializeField] private float wave3length;

    [Header("Wave Callbacks")]
    [SerializeField] private GameObject wave1cb;
    [SerializeField] private GameObject wave2cb;
    [SerializeField] private GameObject wave3cb;
    [SerializeField] private GameObject bosscb;


    [Header("Boss")]
    [SerializeField] private GameObject boss;



    private void Awake()
    {
        if (!instance)
            instance = this;
        else
            Destroy(gameObject);

        Init();
    }


    private void Init()
    {
        phase = EGamePhase.Wave1;
    }

    private void Start()
    {
        StartCoroutine(Level());

        boss.SetActive(false);
    }


    IEnumerator Level()
    {
        yield return new WaitForSeconds(5.0f);
        ActivateCbForPhase(phase);


        while (enabled)
        {
            yield return new WaitForSeconds(wave1length);


            ActivateCbForPhase(EGamePhase.Wave2);

            yield return new WaitForSeconds(2.0f);

            phase = EGamePhase.Wave2;

            yield return new WaitForSeconds(wave2length);

            ActivateCbForPhase(EGamePhase.Wave3);

            yield return new WaitForSeconds(2.0f);

            phase = EGamePhase.Wave3;

            yield return new WaitForSeconds(wave3length);

            ActivateCbForPhase(EGamePhase.Boss);

            yield return new WaitForSeconds(2.0f);

            phase = EGamePhase.Boss;

            boss.SetActive(true);
            SoundsManager.instance.SetBossMusic();

        }
    }

    void ActivateCbForPhase(EGamePhase phase)
    {
        wave1cb.SetActive(false);
        wave2cb.SetActive(false);
        wave3cb.SetActive(false);
        bosscb.SetActive(false);

        if (phase == EGamePhase.Wave1)
            wave1cb.SetActive(true);
        else if (phase == EGamePhase.Wave2)
            wave2cb.SetActive(true);
        else if (phase == EGamePhase.Wave3)
            wave3cb.SetActive(true);
        else if (phase == EGamePhase.Boss)
            bosscb.SetActive(true);

    }


}
