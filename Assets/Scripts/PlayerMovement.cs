using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;


public enum EFireMode { Single, Double}


public class PlayerMovement : MonoBehaviour
{
    public static PlayerMovement instance = null;

    [Header("Movement")]
    [SerializeField] private Camera cam;
    [SerializeField] private float horizontalSpeed;
    [SerializeField] private float verticalSpeed;

    [Header("Fire")]
    [SerializeField] private float fireRate;
    [SerializeField] private GameObject projectilePrefab;
    [SerializeField] private Transform singleTip;
    [SerializeField] private Transform doubleTipLeft;
    [SerializeField] private Transform doubleTipRight;

    private EFireMode fireMode = EFireMode.Single;


    private int defaultDamage = 1;
    private int currentDamage;

    Stopwatch stopwatch;


    void Awake()
    {
        instance = this;
    }


    void Start()
    {
        currentDamage = defaultDamage;

        stopwatch = new Stopwatch();

        stopwatch.Start();
    }

    // Update is called once per frame
    void Update()
    {
        ProcessDirection();

        ProcessFire();
    }

    void ProcessDirection()
    {
        Vector3 screenPos = cam.WorldToScreenPoint(transform.position);

        float offset = Screen.width * 0.10f;

        if (Input.GetKey(KeyCode.LeftArrow) && screenPos.x > offset)
            transform.Translate(Vector3.left * horizontalSpeed * Time.deltaTime);

        if (Input.GetKey(KeyCode.RightArrow) && screenPos.x < (Screen.width - offset))
            transform.Translate(Vector3.right * horizontalSpeed * Time.deltaTime);

        transform.Translate(Vector3.forward * verticalSpeed * Time.deltaTime);
    }

    void ProcessFire()
    {
        if (Input.GetKey(KeyCode.Space) && stopwatch.ElapsedMilliseconds > fireRate)
        {
            if (fireMode == EFireMode.Single)
            {
                SpawnBullet(singleTip.position, currentDamage);
            }
            else if (fireMode == EFireMode.Double)
            {
                SpawnBullet(doubleTipLeft.position, currentDamage);
                SpawnBullet(doubleTipRight.position, currentDamage);
            }
           

            stopwatch.Restart();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        GameObject other = collision.gameObject;

        if (other.tag == "Enemy")
        {
            Enemy e = other.GetComponent<Enemy>();

            e.ApplyDamage(-1);

            GameManager.instance.TakeDamage(15);
        }

        
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PowerUp>())
        {
            PowerUp p = other.gameObject.GetComponent<PowerUp>();

            if (p.type == EPowerUpType.DoubleShoot)
            {
                StopCoroutine(StopDouble());

                fireMode = EFireMode.Double;
                GameManager.instance.ShowWeapon(fireMode);

                StartCoroutine(StopDouble());
            }
            else if (p.type == EPowerUpType.ExtraLife)
            {
                GameManager.instance.AddLife(20);
            }


            p.Explode();
        }
    }


    void SpawnBullet(Vector3 position, int damage)
    {
        GameObject projectile = Instantiate(projectilePrefab, position, Quaternion.identity);

        projectile.GetComponent<Bullet>().Init(damage);

        SoundsManager.instance.PlayFireSound();

    }


    IEnumerator StopDouble()
    {
        yield return new WaitForSeconds(20.0f);

        fireMode = EFireMode.Single;

        GameManager.instance.ShowWeapon(fireMode);
    }
}
