using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CliffSpawner : MonoBehaviour
{
    [SerializeField] private GameObject cliffPrefab;
    [SerializeField] private int ammount;
    [SerializeField] private Transform cliffHolder;

    private float cliffWidth = 20.0f;

    // Start is called before the first frame update
    void Start()
    {
        SpawnCliff();
    }

    void SpawnCliff()
    {
        for(int i = 0; i < ammount; i++)
        {
            GameObject cliff = Instantiate(cliffPrefab, cliffHolder);
            cliff.transform.position = new Vector3(0.0f, 0.0f, i * cliffWidth);
        }
    }
}
