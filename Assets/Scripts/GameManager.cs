using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    [Header("UI")]
    [SerializeField] private UIText scoreText;
    [SerializeField] private Slider healthSlider;

    [Header("Pause")]
    [SerializeField] private GameObject pauseCanvas;

    [Header("GameOver")]
    [SerializeField] private GameObject gameOverCanvas;
    [SerializeField] private TMP_Text gameOverScoreText;


    [Header("GameWon")]
    [SerializeField] private GameObject gameWonCanvas;
    [SerializeField] private TMP_Text gameWonScoreText;

    [Header("PostProcessing")]
    [SerializeField] private GameObject postProcessingVolume;

    [Header("Weapons")]
    [SerializeField] private GameObject singleShoot;
    [SerializeField] private GameObject doubleShoot;


    private int maxHealth = 50;
    private int health;
    private int score;
    private bool isPaused;



    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            DestroyImmediate(gameObject);
    }


    private void Start()
    {
        health = maxHealth;
        score = 0;
        isPaused = false;

        pauseCanvas.SetActive(false);
        gameOverCanvas.SetActive(false);
        gameWonCanvas.SetActive(false);

        postProcessingVolume.SetActive(true);

        UpdateHealthUI();
        
        AddScore(0);
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            OnPause(!isPaused);
    }


    public void TakeDamage(int damageValue)
    {
        health -= damageValue;

        if (health <= 0)
        {
            Invoke("OnShowGameOver", 2.0f);
        }

        UpdateHealthUI();
    }

    void OnShowGameOver()
    {
        ShowGameOver();
    }

    void UpdateHealthUI()
    {
        healthSlider.value = health / (float)maxHealth;
    }

    public void AddLife(int lifeValue)
    {
        if (health + lifeValue > maxHealth)
            health = maxHealth;
        else
            health += lifeValue;

        UpdateHealthUI();
    }

    public void AddScore(int extraScore)
    {
        score += extraScore;

        scoreText.SetText(score.ToString());
    }



    public void OnPause(bool state)
    {
        isPaused = state;

        pauseCanvas.SetActive(state);

        SoundsManager.instance.PauseMusic(state);

        FreezeTime(state);
    }


    private void FreezeTime(bool state)
    {
        Time.timeScale = (state) ? 0 : 1;
    }

    public void OnQuitLevel()
    {
        FreezeTime(false);
        SceneManager.LoadScene("Menu");
    }

    public void OnRestartLevel()
    {
        FreezeTime(false);
        SceneManager.LoadScene("Game");
    }


    private void ShowGameOver()
    {
        gameOverCanvas.SetActive(true);
        FreezeTime(true);
        gameOverScoreText.text = "Your score was " + score.ToString() + " !";
    }

    public void ShowGameWon()
    {
        gameWonCanvas.SetActive(true);
        FreezeTime(true);
        gameWonScoreText.text = "Your score was " + score.ToString() + " !";
    }

    public void ActivatePostProcessing(bool value)
    {
        postProcessingVolume.SetActive(value);
    }


    public void ShowWeapon(EFireMode mode)
    {
        singleShoot.SetActive(false);
        doubleShoot.SetActive(false);

        if (mode == EFireMode.Single)
            singleShoot.SetActive(true);
        else if (mode == EFireMode.Double)
            doubleShoot.SetActive(true);
    }
}
