using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    [SerializeField] private GameObject explosion;

    float speed = 80.0f;
    int damageValue = 2;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, 10.0f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<PlayerMovement>())
        {
            GameManager.instance.TakeDamage(damageValue);

            Explode();
        }
        if (collision.gameObject.GetComponent<Bullet>())
        {
            Bullet b = collision.gameObject.GetComponent<Bullet>();

            b.Explode();

            Explode();
        }
    }

    private void Explode()
    {
        if (explosion)
        {
            GameObject e = Instantiate(explosion, transform.position, transform.rotation);
            Destroy(e, 2.0f);
        }

        Destroy(gameObject);
    }
}
