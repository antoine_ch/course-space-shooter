using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodingEnemy : Enemy
{
    [SerializeField] private float phaseDuration = 1.50f;
    private bool stirShip = false;

    private void Start()
    {
        StartCoroutine(Direction());
    }

    IEnumerator Direction()
    {
        while (enabled)
        {
            yield return new WaitForSeconds(phaseDuration);
            stirShip = true;

            yield return new WaitForSeconds(phaseDuration);
            stirShip = false;
        }
    }

    private void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);

        if (stirShip)
        {
            Vector3 v = transform.position;
            v.x = Mathf.Lerp(v.x, PlayerMovement.instance.transform.position.x, Time.deltaTime);
            transform.position = v;
        }
    }
}
