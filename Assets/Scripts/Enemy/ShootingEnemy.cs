using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingEnemy : Enemy
{
    [Header("ShootingEnemy")]
    [SerializeField] private float fireDelay;
    [SerializeField] private int fireCount;
    [SerializeField] private float fireRate;
    [SerializeField] private GameObject enemyBullet;
    [SerializeField] private List<Transform> shootingPoints;
    [SerializeField] private RandomSound randomSound;


    private void Start()
    {
        StartCoroutine(Shoot());
    }

    IEnumerator Shoot()
    {
        while(enabled)
        {
            
            
            for (int i = 0; i < fireCount; i++)
            {
                randomSound.Play();

                foreach (Transform t in shootingPoints)
                {
                    Instantiate(enemyBullet, t.position, t.rotation);
                }

                yield return new WaitForSeconds(fireRate);
            }

            yield return new WaitForSeconds(fireDelay);

        }
    }
}
