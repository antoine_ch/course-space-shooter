using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BossBall : MonoBehaviour
{
    [SerializeField] private GameObject explosionPrefab;
    [SerializeField] private GameObject tankPrefab;
    [SerializeField] private Animator animator;
    [SerializeField] private MeshRenderer mesh;
    [Header("UI")]
    [SerializeField] private TMP_Text ui;

    private int maxHealth = 10;
    private int health;
    bool ready = false;


    private void Start()
    {
        Invoke("Init", Random.Range(1.0f, 1.8f));
    }

    void Init()
    {
        animator.enabled = true;
        ready = true;
        health = maxHealth;
    }


    private void OnCollisionEnter(Collision collision)
    {
        
            
        if (collision.gameObject.GetComponent<Bullet>())
        {
            Bullet b = collision.gameObject.GetComponent<Bullet>();

            if (ready)
                TakeDamage(1);

            b.Explode();
        }
    }

    private void TakeDamage(int value)
    {
        health -= value;

        ui.text = health.ToString();

        if (health <= 0)
        {
            Kill();
        }

    }

    private void Kill()
    {
        GameObject e = Instantiate(explosionPrefab, transform.position, transform.rotation);

        Vector3 p = transform.position;
        p.y -= 0.2f;

        GameObject t = Instantiate(tankPrefab, p, transform.rotation);

        Destroy(e, 1.0f);
        Destroy(t, 10.0f);

        ready = false;
        mesh.enabled = false;

        Boss.instance.RemoveBall();

        StartCoroutine(ResetBall());
    }

    IEnumerator ResetBall()
    {
        yield return new WaitForSeconds(19.0f);

        mesh.enabled = true;
        animator.SetTrigger("reset");
        ready = true;
        health = maxHealth;
        ui.text = health.ToString();

        Boss.instance.AddBall();
    }

}
