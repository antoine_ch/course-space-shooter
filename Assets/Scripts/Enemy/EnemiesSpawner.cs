using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


[System.Serializable]
public class EnemyOption
{
    public bool wave1;
    public bool wave2;
    public bool wave3;
    public bool boss;

    public GameObject prefab;
}



public class EnemiesSpawner : Spawner
{
    [Header("EnemiesSpawner")]

    [SerializeField] private List<EnemyOption> enemyPrefabs;
    [Header("Waves Spawn Rates")]
    [SerializeField] private float wave1spawnDelay;
    [SerializeField] private float wave2spawnDelay;
    [SerializeField] private float wave3spawnDelay;
    [SerializeField] private float bossspawnDelay;





    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnCoroutine());

    }

    IEnumerator SpawnCoroutine()
    {
        yield return new WaitForSeconds(5.0f);


        while (LevelManager.instance.phase != EGamePhase.End)
        {
            EGamePhase phase = LevelManager.instance.phase;

            Vector3 randomStartPos = GetRandomSpawnPos();

            GameObject enemyPrefab = GetPrefabForPhase(phase);

            if (enemyPrefab != null)
                Instantiate(enemyPrefab, randomStartPos, spawnParent.rotation);

            yield return new WaitForSeconds(GetSpawnDelayForPhase(phase));
        }
    }

    
    GameObject GetPrefabForPhase(EGamePhase gamePhase)
    {
        List<EnemyOption> shortList = null;

        if (gamePhase == EGamePhase.Wave1)
            shortList = enemyPrefabs.Where(o => o.wave1 == true).ToList();
        else if (gamePhase == EGamePhase.Wave2)
            shortList = enemyPrefabs.Where(o => o.wave2 == true).ToList();
        else if (gamePhase == EGamePhase.Wave3)
            shortList = enemyPrefabs.Where(o => o.wave3 == true).ToList();
        else if (gamePhase == EGamePhase.Boss)
            shortList = enemyPrefabs.Where(o => o.boss == true).ToList();

        if (shortList == null)
            return null;

        return shortList[Random.Range(0, shortList.Count)].prefab;
    }

    float GetSpawnDelayForPhase(EGamePhase gamePhase)
    {
        if (gamePhase == EGamePhase.Wave1)
            return wave1spawnDelay;
        else if (gamePhase == EGamePhase.Wave2)
            return wave2spawnDelay;
        else if (gamePhase == EGamePhase.Wave3)
            return wave3spawnDelay;
        else if (gamePhase == EGamePhase.Boss)
            return bossspawnDelay;


        return 5.0f;
    }
}
