using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretTrigger : MonoBehaviour
{
    [SerializeField] private GameObject explosion;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerMovement>())
        {
            GameManager.instance.TakeDamage(15);

            GameObject e = Instantiate(explosion, other.transform.position, Quaternion.identity);
            Destroy(e, 1.5f);
        }
    }
}
