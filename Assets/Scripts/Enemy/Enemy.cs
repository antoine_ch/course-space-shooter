using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [Header("Enemy")]
    [SerializeField] private GameObject explosionPrefab;
    [SerializeField] protected int healthPoints = 2;

    [SerializeField] protected float speed = 5.0f;
    
    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
    }

    public void ApplyDamage(int damage)
    {
        if (damage == -1)
        {
            DestroyShip();
            return;
        }


        healthPoints -= damage;
        
        if (healthPoints <= 0)
        {
            DestroyShip();

            GameManager.instance.AddScore(100);
        }
    }

    private void DestroyShip()
    {
        GameObject explosion = Instantiate(explosionPrefab, transform.position, transform.rotation);
        Destroy(explosion, 1.0f);

        Destroy(gameObject);
    }
}
