using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    public static Boss instance = null;

    private int ballToKill;

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        ballToKill = 4;
    }

    public void RemoveBall()
    {
        ballToKill--;

        if (ballToKill == 0)
        {
            Invoke("OnBossKilled", 2.0f);
        }
    }

    public void AddBall()
    {
        ballToKill++;
    }


    void OnBossKilled()
    {
        GameManager.instance.ShowGameWon();
    }
    
}
