using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Turret : MonoBehaviour
{
    [SerializeField] private Animator animator;

    [SerializeField] private GameObject triggers;
    [SerializeField] private List<Animator> firePoints;
    [SerializeField] private Transform holder;


    float loadTime = 2.0f;

    GameObject player;
    bool ready = false;

    bool dontLook = false;


    void Start()
    {
        triggers.SetActive(false);

        Invoke("Init", 1.0f);
        player = PlayerMovement.instance.gameObject;

        Invoke("SetReady", 1.6f);

    }
    void Init()
    {
        animator.enabled = true;
    }

    void SetReady()
    {
        ready = true;

        StartCoroutine(FireCoroutine());
    }


    private void Update()
    {
        if (ready && !dontLook)
        {
            Vector3 p = player.transform.position;
            p.y = holder.position.y;

            holder.LookAt(p);

        }

    }

    IEnumerator FireCoroutine()
    {
        while(enabled)
        {

            dontLook = true;

            yield return new WaitForSeconds(loadTime);


            for (int i = 0; i < firePoints.Count; i++)
            {
                yield return new WaitForSeconds(Random.Range(0.2f, 0.5f));
                firePoints[i].SetBool("laser", true);
            }

            triggers.SetActive(true);

            yield return new WaitForSeconds(2.0f);

            triggers.SetActive(false);

            for (int i = 0; i < firePoints.Count; i++)
            {
                firePoints[i].SetBool("laser", false);

            }

            dontLook = false;

            yield return new WaitForSeconds(7.0f);
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    void Shoot()
    {

    }
}
