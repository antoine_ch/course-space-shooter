using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    [SerializeField] private Animator menuAnimator;

    public void OnStartClick()
    {
        SceneManager.LoadScene("Game");
    }

    public void OnCreditClick() 
    {
        // TODO: Show Credits
        menuAnimator.SetBool("showCredits", true);
    }

    public void OnCreditClose()
    {
        menuAnimator.SetBool("showCredits", false);
    }

    public void OnQuitClick()
    {
        Application.Quit();
    }
}
