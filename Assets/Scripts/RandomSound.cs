using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSound : MonoBehaviour
{
    [SerializeField] private bool playOnStart = true;
    [SerializeField] private ESoundType type;
    [SerializeField] private List<AudioClip> clips;
    [SerializeField] private AudioSource source;

    // Start is called before the first frame update
    void Start()
    {
        if (playOnStart)
            Play();
    }

    public void Play()
    {
        source.volume = SoundsManager.instance.GetSoundVolume(type);
        source.clip = clips[Random.Range(0, clips.Count)];
        source.Play();
    }
}
