using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum EPowerUpType { DoubleShoot, ExtraLife }

public class PowerUp : MonoBehaviour
{
    [SerializeField] private GameObject explosionPrefab;
    public EPowerUpType type;

    private float speed = 5.0f;

    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
    }

    public void Explode()
    {
        GameObject explosion = Instantiate(explosionPrefab, transform.position, transform.rotation);
        Destroy(explosion, 1.0f);
        Destroy(gameObject);
    }
}
