using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeSpawner : MonoBehaviour
{
    [Header("Trees")]
    [SerializeField] private List<GameObject> treePrefabs = new List<GameObject>();
    [SerializeField] private Transform treesParent;

    [Header("Bounds")]
    [SerializeField] private Transform bottomLeft;
    [SerializeField] private Transform bottomRight;
    [SerializeField] private Transform topLeft;
    [SerializeField] private Transform topRight;

    [Header("Params")]
    [SerializeField] private int ammount;
    [SerializeField] private float laneWidth;

    // Start is called before the first frame update
    void Start()
    {
        PopulateWithTrees();   
    }

    void PopulateWithTrees()
    {
        for (int i = 0; i < ammount; i++)
        {
            float randomX = Random.Range(bottomLeft.position.x, -laneWidth / 2.0f) * ((Random.Range(0, 2) == 0) ? -1 : 1);

            Vector3 randomPos = new Vector3(randomX, 0.0f, Random.Range(bottomLeft.position.z, topLeft.position.z));

            float randomScale = Random.Range(0.9f, 1.3f) *  Mathf.Clamp01(0.5f + (Mathf.Abs(randomX) / bottomRight.position.x));

            SpawnRandomTreeAtPos(randomPos, randomScale);
        }
    }

    void SpawnRandomTreeAtPos(Vector3 pos, float randomScale)
    {
        GameObject prefab = treePrefabs[Random.Range(0, treePrefabs.Count)];

        GameObject tree = Instantiate(prefab, treesParent);
        
        tree.transform.position = pos;
    }
}
