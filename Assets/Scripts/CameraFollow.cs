using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform target;

    private Vector3 prevTargetPos;

    private void Start()
    {
        prevTargetPos = target.position;
    }

    void LateUpdate()
    {
        Vector3 currentPos = target.position;

        currentPos.x = 0;

        transform.position = currentPos; //Vector3.Lerp(prevTargetPos, currentPos, 0.5f * Time.deltaTime);

        prevTargetPos = currentPos;
    }
}
